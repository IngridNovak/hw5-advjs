const postsUrl = 'https://ajax.test-danit.com/api/json/posts';
const usersUrl = 'https://ajax.test-danit.com/api/json/users';
class Card{
    constructor(id, userName,userEmail, postTitle, postBody) {
        this.id = id;
        this.userName = userName;
        this.userEmail = userEmail;
        this.title = postTitle;
        this.body = postBody;

    }
}

class User{
    constructor(id,userName, email) {
        this.id = id;
        this.userName = userName;
        this.email = email;
    }
}
class Post{
    constructor(id,userId, title, body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }
}

function checkResponse(url, method) {
    return fetch(url,{
        method: method,
    })
        .then(response => {
            if (!response.ok) {
                throw new Error("Помилка звязку");
            }
            if(method !== "GET") {
            return true;
            }
                return response.json()

        }).catch(err => {
            console.log(err)
        })
}

function loadPosts(){
    return checkResponse(postsUrl,'GET')
    .then(posts => {
        let postsArray = [];
        posts.forEach(post => {
           let postObj = new Post(post.id, post.userId, post.title, post.body);
           postsArray.push(postObj);
        })
       return postsArray;
    }).catch(err => {
        console.error(`Error on loadPosts: ${err}`);
        })
}
function loadUsers(){
    return checkResponse(usersUrl,'GET')
    .then(users => {
        let usersArray = [];
        users.forEach(user => {
            let userObj = new User(user.id, user.name, user.email);
            usersArray.push(userObj);
            })
        return usersArray;
    }).catch(err => {
            console.error(`Error on LoadUsers: ${err}`);
        })
}
function getData(){
    Promise.all([loadUsers(), loadPosts()]).then(responses =>{
        let users = responses[0];
        let posts = responses[1];
        let cards = [];
        posts.forEach(post => {
            let userObj;
            users.forEach(user => {
                if(user.id === post.userId){
                    userObj = user;
                }
            })
            let card = new Card(post.id,userObj.userName, userObj.email, post.title, post.body);
            cards.push(card);
        })

        showCards(cards);
    })
}

function showCards(cards){
    cards.forEach(card => {
        let cardElem = createDOMElement('div', {'class': 'card', 'id': 'card-' + card.id}, document.getElementById('cards'));

        let button = createDOMElement('div', {'id': 'button'}, cardElem);
        createDOMElement('button', {'class': "button-15",'id': `deleteButton-`+ card.id , 'body': 'Delete post'}, button)
            .addEventListener('click', deletePost);

        let postColumn = createDOMElement('div', {'id': "post_column"}, cardElem);
        let userInfo = createDOMElement('div', {'id': "user_info"}, postColumn);
        createDOMElement('div', {'id': 'userName', 'body': card.userName}, userInfo);
        createDOMElement('div', {'id': 'email', 'body': card.userEmail}, userInfo);

        let postElem = createDOMElement('div', {'id': "post"}, postColumn);
        createDOMElement('div', {'id': "title",'body': card.title}, postElem);
        createDOMElement('div', {'id': "body",'body': card.body}, postElem);
    })
}


function deletePost(button){
    let postId= button.target.id.split('-')[1];
   checkResponse(postsUrl + '/' + postId, 'DELETE')
       .then(response => {
           if(response === true){
               document.getElementById('card-' + postId).remove();

           }

           }
       )

}

function createDOMElement(tag, params, parentElement) {
    let elem = document.createElement(tag);
    for (const key in params) {
        elem.setAttribute(key, params[key]);
        if (key === 'body') {
            elem.innerHTML = params[key];
        }
    }
    if (parentElement !== undefined) {
        parentElement.append(elem);
    }
    return elem;
}



getData()